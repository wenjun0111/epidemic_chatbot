class IdModel():
    def __init__(self, lineId="1", chatBotId='1'):
        self.lineId = lineId
        self.chatBotId = chatBotId
class UserModel():
    def __init__(self, name=None, phone=None, email=None, role="customer", lineId="1"):
        self.name = name
        self.phone = phone
        self.email = email
        self.role = role
        self.lineId = lineId
class SiteModel():
    def __init__(self, name=None, city=None, district=None, detail=None , lineId="1"):
        self.name = name
        self.address = {
            "city":city,
            "district":district,
            "detail":detail,
        }
        self.lineId = lineId
class SiteResponse():
    def __init__(self, siteModel=None, lineId="1"):
        self.siteModel = siteModel
        self.lineId = lineId
        self.result  = { 
            "title":None,
            "description":None,
        }
class UserResponse():
    def __init__(self, userModel=None, uid_list=None, code=None):
        self.userModel = userModel
        self.uid_list = uid_list
        self.result  = { 
            "code":code,
            "title":None,
            "description":None,
        }
class RichMenuModel():
    def __init__(self,phone = None, lineId="1", role="customer", menuId=None):#, submenuId=None
        if phone != None:
            phone = phone.split('+')[1]
        self.phone = phone
        self.lineId = lineId
        self.role = role
        self.jsonPath = None
        self.imagePath = None
        self.menu = {"aliasId":None,"Id":menuId}
        #self.submenu = {"aliasId":None,"Id":submenuId}
class FootPrintModel():
    def __init__(self, siteId=None, lineId="1", timestamp = None):
        self.siteId = siteId
        self.lineId = lineId
        self.timestamp = timestamp
class FootPrintResponse():
    def __init__(self, footPrintModel=None, code=None):
        self.footPrintModel = footPrintModel
        self.result  = { 
            "code":code,
            "title":None,
            "description":None,
        }        
