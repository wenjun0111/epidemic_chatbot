import firebaseConfig
import firebase_admin
from firebase_admin import credentials

cred = credentials.Certificate(firebaseConfig.keyFile)
app=firebase_admin.initialize_app(cred, {'storageBucket': firebaseConfig.storageBucket})
