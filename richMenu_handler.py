import json
import requests
import argparse
from linebot import LineBotApi
from model import RichMenuModel
url = "https://b87b6026ad5e.ngrok.io"
# please fill your chatbot channel access token in the below variable
CHANNEl_ACCESS_TOKEN = 'chatbotToken'
lineBotApi = LineBotApi(CHANNEl_ACCESS_TOKEN)
#-------------------------------------------------------------------------#
def dynamic_create_richMenu(richMenuModel):
    phone = richMenuModel["phone"]
    role = richMenuModel["role"]

    richMenuModel["jsonPath"] = "richmenu/"+ role + "/" + "richmenu_"+ role +"_1.json"
    richMenuModel["imagePath"] = "richmenu/"+ role + "/" + "richmenu_"+ role +"_1.png"  

    richMenuModel = create_and_bind_richMenu(richMenuModel)
    if richMenuModel["menu"]["aliasId"] == None:
        richMenuModel["menu"]["aliasId"] = phone
    update_richMenu(richMenuModel)
    return richMenuModel
#-------------------------------------------------------------------------#
def create_and_bind_richMenu(richMenuModel): 
    # create rich menu of the chatbot
    #phone = richMenuModel["phone"]
    lineId = richMenuModel["lineId"]
    jsonPath = richMenuModel["jsonPath"]
    jsonFile = open(jsonPath, encoding='utf-8')
    menuJsonObject = json.load(jsonFile)
    #-
    for i in range(0,6):
        route = menuJsonObject["areas"][i]["action"]["data"]
        menuJsonObject["areas"][i]["action"]["uri"] = url + route + lineId
    #menuJsonObject["areas"][-1]["action"]["richMenuAliasId"] = phone + "-a"
    print(menuJsonObject)
    #-
    
    createResponse = requests.post(
        'https://api.line.me/v2/bot/richmenu',
        json=menuJsonObject,
        headers={'Authorization': 'Bearer {}'.format(CHANNEl_ACCESS_TOKEN)}
    )
    print(createResponse.text)
    richMenuId = json.loads(createResponse.text)['richMenuId']
    print('Create rich menu completed !')

    # upload image of the rich menu
    imagePath = richMenuModel["imagePath"]
    richMenuImageFile = open(imagePath, 'rb')
    contentType = 'image/{}'.format(richMenuImageFile.name.split('.')[-1])
    lineBotApi.set_rich_menu_image(richMenuId, contentType, richMenuImageFile)
    #----
    if richMenuModel["menu"]["Id"] ==None:
        richMenuModel["menu"]["Id"] = richMenuId
    #----
    print('Upload image completed !')
    print('All Finish...... Rich menu ID is {} !!'.format(richMenuId))

    return richMenuModel
"====================="
def set_default_rich_menu(richMenuId):
    lineBotApi.set_default_rich_menu(richMenuId)
    print(f'Success to set {richMenuId} as default rich menu !')
"====================="
def update_richMenu(richMenuModel):
    lineId = richMenuModel["lineId"]
    role = richMenuModel["role"]
    richMenuId = richMenuModel["menu"]["Id"]
    headers={'Authorization': 'Bearer {}'.format(CHANNEl_ACCESS_TOKEN)}
    requests.post('https://api.line.me/v2/bot/user/' + lineId + '/richmenu/' + richMenuId , headers=headers)  
    print(f'Success to update {lineId} as '+ role +' rich menu !')    
"====================="
def create_richMenu_alias(aliasId, richMenuId):
    body = {
        'richMenuAliasId': aliasId,
        'richMenuId': richMenuId
    }
    createResponse = requests.post(
        'https://api.line.me/v2/bot/richmenu/alias',
        json=body,
        headers={'Authorization': 'Bearer {}'.format(CHANNEl_ACCESS_TOKEN)}
    )
    print('Create alias completed !')
"====================="
def delete_rich_menu(richMenuId):
    print(richMenuId)
    if (richMenuId == 'all'):
        richMenuList = lineBotApi.get_rich_menu_list()
        for richMenu in richMenuList:
            lineBotApi.delete_rich_menu(richMenu.rich_menu_id)
        
        richMenuAliasList = list_richMenu_alias()
        for richMenuAlias in richMenuAliasList:
            delete_richMenu_alias(richMenuAlias['richMenuAliasId'])

    else:
        lineBotApi.delete_rich_menu(richMenuId)
        # richMenuAliasList = list_richMenu_alias()
        # print(richMenuAliasList)
        # richMenuAliasToDelete = list(filter(lambda richMenuAlias: richMenuAlias['richMenuId']==richMenuId, richMenuAliasList))[0]
        # delete_richMenu_alias(richMenuAliasToDelete['richMenuAliasId'])
    print('Delete rich menu completed !!')
"====================="
def delete_richMenu_alias(aliasId):
    apiUrl = f'https://api.line.me/v2/bot/richmenu/alias/{aliasId}'
    deleteResponse = requests.delete(
        apiUrl,
        headers={'Authorization': 'Bearer {}'.format(CHANNEl_ACCESS_TOKEN)}
    )
"====================="

# list Rich menu IDs that bind to chatbot so far
def list_rich_menu():
    richMenuList = lineBotApi.get_rich_menu_list()
    if (len(richMenuList) > 0):
        print('Rich menus that you have created at the channel are below......')
        for richMenu in richMenuList:
            print(richMenu.rich_menu_id)
    else:
        print('No any rich menu that you have created at the channel !!')
"====================="
def list_richMenu_alias():
    listResponse = requests.get(
        'https://api.line.me/v2/bot/richmenu/alias/list',
        headers={'Authorization': 'Bearer {}'.format(CHANNEl_ACCESS_TOKEN)}
    )

    return json.loads(listResponse.text)['aliases']

