import os
from types import MethodType
from flask import Flask, request, render_template, jsonify
import QRcodeGenerate
from firebaseApi import Firebase
import requests
import json
import time
from model import UserModel,UserResponse,RichMenuModel,SiteModel,SiteResponse,FootPrintModel,FootPrintResponse,IdModel
import simpleFirebaseAuthProxy as auth
from simpleFirebaseAuthProxy import getAuthUsers,createUser,deleteUser,updateUser_s,getAuthUserByemail,getuidFromuserData,getlineidFromuserData,getAuthUserById
import liffConfig
import datetime
import cloudFunctionCaller
import threading
from edgePub import edgePub
#import umsConfig
app = Flask(__name__)

userModel = UserModel()
siteModel = SiteModel()
userResponse = UserResponse()
siteResponse = SiteResponse()
footPrintResponse = FootPrintResponse()
firebase = Firebase()
idModel = IdModel()

@app.route("/create", methods=['GET', 'POST'])
def create():
    global userModel
    if request.method == "GET":

        return render_template("simpleRequest.html", userModel=userModel, liffId=liffConfig.liffId)
    if request.method == 'POST':
        print(request.values)
        userModel = UserModel(request.values['name'], request.values['phone'],
                                  request.values['email'], 'customer', request.values['lineId'])
        userModel_dict = userModel.__dict__
        userModel_dict['id'] = request.values['lineId']
        richMenuModel = RichMenuModel(request.values['phone'], request.values['lineId'], 'customer')
        importTime = int(time.time() + 28800)

        firebase.createUser(userModel_dict, importTime)
        bool , error_msg = createUser(userModel_dict , richMenuModel.__dict__)
        if bool:#cloudfunction回覆成功時
            userResponse = UserResponse(userModel = userModel_dict , code = "1").__dict__
            userResponse["result"]["title"] = "註冊成功"
            return render_template("simpleResponse.html", result=userResponse)
        else:
            if error_msg == 'phone':
                description = "不對的手機格式，要用 +886 "
            elif error_msg == 'email':
                description = "不對的信箱格式，請再重註冊 "
            userResponse = UserResponse(userModel = userModel.__dict__ , code = "-1").__dict__
            userResponse["result"]["title"] = "註冊失敗"
            userResponse["result"]["description"] = description
            return render_template("simpleFail.html", result=userResponse)
#----------------------個人資料-----------------------------------------
@app.route("/query",methods=['GET'])
def query():
    lineId = request.values.get('lineId')
    print("---query----lineId-----------")
    print(lineId)  
    title = "我的個資"
    userData = firebase.getUserDataById(lineId)
    userData['timestamp'] = str(datetime.datetime.utcfromtimestamp(userData['timestamp']).strftime('%Y-%m-%d %H:%M:%S'))
    return render_template('queryUserOfFirestoreResponse.html', result = userData, title = title)
#----------------------商店相關-----------------------------------------
@app.route("/createsite",methods=['GET', 'POST'])
def createsite():
    if request.method == "GET":
        return render_template("createsiteRequest.html", liffId=liffConfig.liffId)
    if request.method == "POST":
        print(request.values)
        siteModel = SiteModel(request.values['name'], request.values['city'], request.values['district'], request.values['detail'], request.values['lineId'])   
        siteModel_dict = siteModel.__dict__
        importTime = int(time.time() + 28800)
        siteModel_dict['timestamp'] = importTime
        siteId = firebase.createSite(siteModel_dict)
        #產生Qrcode
        qrcodeGenerate = QRcodeGenerate.QrcodeGenerate()
        qrCode = qrcodeGenerate.generate(siteId)
        #將Qrcode存至firebase storage
        qrCodeUrl = firebase.uploadImage(qrCode, siteId)
        siteModel_dict['qrCodeUrl'] = qrCodeUrl
        #將Qrcode url存進site
        firebase.addQrCodeUrlToSite(siteId,qrCodeUrl)
        #將剛剛建立的site's Id 存至建立者的site collection內
        firebase.addSiteIdToUser(siteId,siteModel_dict['lineId'])

        if siteId != None:#cloudfunction回覆成功時
            siteResponse = SiteResponse(siteModel = siteModel.__dict__ ).__dict__
            siteResponse["result"]["title"] = "商店註冊成功"
            #chatbot推播
            url = "https://asia-east1-ntut-chatbot.cloudfunctions.net/pushMessage"
            mySiteSize = len(firebase.listSitesDataOfUser(siteModel_dict['lineId']))
            receiverLineIdList = [siteModel_dict['lineId']]
            notificationModel = {
                "providerId": 'klT8tZCRzYqAIfZ3O7jM',
                "receiverLineIdList":receiverLineIdList,
                "messages": [
                    {
                        "messageType": "textTemplate",
                        "content": "商店註冊成功\n" \
                                f"商店名稱: {siteModel_dict['name']}\n" \
                                f"時間: {str(datetime.datetime.utcfromtimestamp(importTime).strftime('%Y-%m-%d %H:%M:%S'))}\n" \
                                f"商店總數: {mySiteSize}" 
                    }
                ]
            }

            print(notificationModel)
            notificationThread =threading.Thread(target = cloudFunctionCaller.cloudFunctionCallerHttp,kwargs=dict(body = notificationModel,url=url))
            notificationThread.start()
            # cloudFunctionCaller.cloudFunctionCallerHttp(body = notificationModel,url=url)

            #bigquery寫入
            bigquery_url = 'https://asia-east2-bqtestproject-322205.cloudfunctions.net/bqDataModel'
            bigquery_dict ={'siteData': 
                                {
                                'siteId':siteId,
                                'siteName':siteModel_dict['name'],
                                'infection':0,
                                'timestamp':siteModel_dict['timestamp'],
                                }
                            }
            # writeInbigquery =threading.Thread(target = cloudFunctionCaller.cloudFunctionForBigQueryCallerHttp,kwargs=dict(body = bigquery_dict,url=bigquery_url))
            # writeInbigquery.start()
            bigquery1Thread =  threading.Thread(target = edgePub, args = (bigquery_dict,))
            bigquery1Thread.start()

            return render_template("createsiteResponse.html", result=siteResponse) 
@app.route("/myShop",methods=['GET'])
def myShop():
    lineId = request.values.get('lineId')
    print("---myShop----lineId-----------")
    print(lineId)  
    title = "商店列表"
    sitesDataOfUser = firebase.listSitesDataOfUser(lineId)
    sitesDataOfUser.sort(key=lambda k: (k.get('timestamp', 0)))
    for sitesData in sitesDataOfUser:
        address = sitesData['address']
        sitesData['address'] = address['city'] + address['district'] + address['detail']
        sitesData['timestamp'] = str(datetime.datetime.utcfromtimestamp(sitesData['timestamp']).strftime('%Y-%m-%d %H:%M:%S'))
    return render_template('sitelistResponse.html', result = sitesDataOfUser, title = title)
#----------------------掃碼-----------------------------------------------
@app.route("/scanQRCode",methods=['GET'])
def scanQRCode():
    lineId = request.values.get('lineId')
    print("---scanQrCode----lineId-----------")
    print(lineId)

    return render_template('scanQrCodeRequest.html', lineId=lineId)
    # return render_template("photo.html")
# ----------------------------掃碼足跡紀錄-----------------------------
@app.route("/footPrintRecord", methods=['POST'])
def recordFootPrint():
    footPrintData = json.loads(request.get_data())
    print(footPrintData)
    importTime = int(time.time() + 28800)
    siteData = firebase.getSiteDataById(footPrintData['siteId'])
    if siteData != None:
        footPrintModel = FootPrintModel(footPrintData['siteId'], footPrintData['lineId'], importTime )
        footPrintModel_dict = footPrintModel.__dict__
        footPrintModel_dict['userId'] = footPrintData['lineId']
        footPrintId = firebase.createFootPrint(footPrintModel_dict)
        firebase.addFootPrintIdToUser(footPrintId, footPrintData['lineId'])
        firebase.addFootPrintIdToSite(footPrintId, footPrintData['siteId'])
        if footPrintId is not None:

            #chatbot推播
            url = "https://asia-east1-ntut-chatbot.cloudfunctions.net/pushMessage"
            receiverLineIdList = [footPrintModel_dict['lineId']]
            notificationModel = {
                "providerId": 'klT8tZCRzYqAIfZ3O7jM',
                "receiverLineIdList":receiverLineIdList,
                "messages": [
                        {
                            "messageType": "textTemplate",
                            "content": "掃碼成功\n" \
                                    f"商店名稱: {siteData['name']}\n" \
                                    f"時間: {str(datetime.datetime.utcfromtimestamp(importTime).strftime('%Y-%m-%d %H:%M:%S'))}\n" \
                        }
                ]
            }

            print(notificationModel)
            notificationThread =threading.Thread(target = cloudFunctionCaller.cloudFunctionCallerHttp,kwargs=dict(body = notificationModel,url=url))
            notificationThread.start()
            # cloudFunctionCaller.cloudFunctionCallerHttp(body = notificationModel,url=url)
            
            #bigquery寫入
            userName = firebase.getUserDataById(footPrintModel_dict['lineId'])['name']
            siteData = firebase.getSiteDataById(footPrintModel_dict['siteId'])
            bigquery_url = 'https://asia-east2-bqtestproject-322205.cloudfunctions.net/bqDataModel'
            bigquery_dict ={'create': 
                                {
                                'userId':footPrintModel_dict['lineId'],
                                'userName':userName,
                                'siteId':footPrintModel_dict['siteId'],
                                'siteName':siteData['name'],
                                'city': siteData['address']['city'],
                                'district':siteData['address']['district'],
                                'address':siteData['address']['detail'],
                                'footprintId':footPrintId,
                                'timestamp':importTime,
                                'infection':0
                                }
                            }
            # writeInbigquery =threading.Thread(target = cloudFunctionCaller.cloudFunctionForBigQueryCallerHttp,kwargs=dict(body = bigquery_dict,url=bigquery_url))
            # writeInbigquery.start()
            bigquery1Thread =  threading.Thread(target = edgePub, args = (bigquery_dict,))
            bigquery1Thread.start()

            return jsonify(footPrintData['siteId'] + '到店掃碼成功')
    else:
        return jsonify('無此商家...')
# ----------------------------User掃碼足跡紀錄-----------------------------
@app.route("/footPrintlist", methods=['GET'])
def footPrintlist():
    lineId = request.values.get('lineId')
    print("---footPrintlist----lineId-----------")
    print(lineId)
    siteDataList = firebase.getAllSiteData()
    site_siteId_dict ={}
    for sitedata in siteDataList:
        site_siteId_dict[ sitedata["id"] ] = sitedata["name"]
    footPrintsDataOfUser = firebase.listFootPrintsDataOfUserByTime(lineId, 0)
    footPrintsDataOfUser.sort(key=lambda k: (k.get('timestamp', 0)))
    for footPrintsData in footPrintsDataOfUser :
        footPrintsData['timestamp'] = str(datetime.datetime.utcfromtimestamp(footPrintsData['timestamp']).strftime('%Y-%m-%d %H:%M:%S'))
        siteId = footPrintsData['siteId']
        footPrintsData['siteName'] = site_siteId_dict[siteId]
    
    footPrintResponse = FootPrintResponse(footPrintModel = footPrintsDataOfUser, code = "1").__dict__
    footPrintResponse["result"]["title"] = "足跡列表"
    return render_template('footPrintlistResponse.html', result = footPrintResponse)
# ----------------------------我的傳播組態設定-----------------------------
@app.route("/mySpreadRequest", methods=['GET'])
def requestMySpread():
    lineId = request.values.get('lineId')
    print("---footPrintlist----lineId-----------")
    print(lineId)
    userData = firebase.getUserDataById(lineId)
    providerData = firebase.getProviderDataById(userData['providerId'])
    provider = {
        'id':providerData['id'],
        'name':providerData['name']
    }

    return render_template('mySpreadRequest.html', myProfile=userData, providerData = provider)
# ----------------------------我的傳播結果-----------------------------
@app.route("/mySpreadResponse", methods=['POST'])
def responseMySpread():
    inputData = {
        'userId': request.values['lineId'],
        'spreadStrength': int(request.values['spreadStrength']),
        'confirmedTime': time.mktime(datetime.datetime.strptime(request.values['confirmedTime'], "%Y-%m-%dT%H:%M:%S").timetuple()) + 28800,
        'contactTracingTime' : int(request.values['contactTracingTime']),
        'epidemicNotification':True
    }
    print(inputData)
    print(request.values['confirmedTime'])

    epidemicPrevention_url = 'https://asia-east1-ntut-chatbot.cloudfunctions.net/epidemicPrevention'
    mySpreadResponse = cloudFunctionCaller.cloudFunctionCallerHttp( body = inputData, url=epidemicPrevention_url)
    print(mySpreadResponse)
    mySpreadResponse = json.loads(mySpreadResponse.text)

    config = {
        'confirmedTime':str(datetime.datetime.utcfromtimestamp(inputData['confirmedTime']).strftime('%Y-%m-%d %H:%M:%S')),
        'spreadStrength': inputData['spreadStrength']
    }

    print(config)
    userData = firebase.getUserDataById(request.values['lineId'])
    providerData = firebase.getProviderDataById(userData['providerId'])
    provider = {
        'id':providerData['id'],
        'name':providerData['name']
    }
    return render_template('mySpreadResponse.html', mySpreadResponse = mySpreadResponse, config=config, providerData = provider)
#========================================================#
port = int(os.environ.get('PORT', 5000))
if __name__ == '__main__':
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.jinja_env.auto_reload = True
    print(time.time()+28800)
    app.run(threaded=True, host='127.0.0.1', port=port)
