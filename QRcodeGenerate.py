import qrcode
from PIL import Image
import io
# pip install qrcode[pil]
# pip install pillow


class QrcodeGenerate():
    def __init__(self):
        self.img = qrcode.make('')
        self.QRcode = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_H,
            box_size=10,
        border=4,
        )

        
    def generate(self,data):
        self.QRcode.add_data(data)
        self.QRcode.make(fit=True)
        self.img = self.QRcode.make_image(fill_color="black", back_color="white").convert('RGB')
        # img_byte = io.BytesIO()
        # self.img.save(img_byte, format="png")
        
        return self.img


# data = "uuid"
# qrcodeGenerate = QrcodeGenerate()
# qrcodeGenerate.generate(data)

    
