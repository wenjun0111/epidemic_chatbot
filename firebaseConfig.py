keyFile = 'firebaseKey.json'
storageBucket = 'ntut-chatbot.appspot.com'
projectName = 'chatbot_covid-19'
# projectName = 'TestingData'
dbName = 'EpidemicPreventionDB'
siteTable = 'sites'
userTable =  'users'
providerTable = 'providers'
footPrintTable = 'footPrints'