import firebaseInit
from firebase_admin import credentials, firestore, initialize_app, storage
import firebaseConfig
import cv2
import numpy
import base64

class Firebase:
    def __init__(self):
        # self.__cred = credentials.Certificate(firebaseConfig.keyFile)
        # initialize_app(self.__cred, {'storageBucket': firebaseConfig.storageBucket})
        self.__firestore = firestore.client()
        self.__myDb = self.__firestore.collection(firebaseConfig.projectName).document(firebaseConfig.dbName)
        self.__sitesCollection = self.__myDb.collection(firebaseConfig.siteTable)
        self.__usersCollection = self.__myDb.collection(firebaseConfig.userTable)
        self.__providersCollection = self.__myDb.collection(firebaseConfig.providerTable)
        self.__footPrintsCollection = self.__myDb.collection(firebaseConfig.footPrintTable)
    
    def createFootPrint(self, footPrintModel):
        response = self.__footPrintsCollection.add(footPrintModel)[1]
        self.__footPrintsCollection.document(response.id).update({'id':response.id})
        return response.id

    def createSite(self, siteModel):
        response = self.__sitesCollection.add(siteModel)[1]
        self.__sitesCollection.document(response.id).update({'id':response.id})
        return response.id

    def addQrCodeUrlToSite(self,siteId, qrCodeUrl):
        self.__sitesCollection.document(siteId).update({'qrCodeUrl':qrCodeUrl})

    def listFootPrintsDataOfSite(self, siteId):
        footPrintIdsOfSite = list(doc.id for doc in self.__sitesCollection.document(siteId).collection('footPrintIds').stream())
        footPrintsDataOfSite = []
        for footPrintId in footPrintIdsOfSite:
            footPrintsDataOfSite.append(self.__footPrintsCollection.document(footPrintId).get().to_dict())

        return footPrintsDataOfSite

    # def createUser(self, userData):
    #     response = self.__usersCollection.add(userData)[1]
    #     self.__usersCollection.document(response.id).update({'id':response.id})
    #     return response.id
    def createUser(self, userModel, importTime):
        id = userModel['lineId']
        user = self.__usersCollection.document(id).get().to_dict()
        if user == None:
            self.__usersCollection.document(id).set(userModel)
            self.__usersCollection.document(id).update({'timestamp':importTime})
    

    def retrieveCustomerDataOfProvider(self, providerId):
        customerList = list(doc.to_dict() for doc in self.__usersCollection.where('providerId', '==', providerId).where('role','==','customer').get())
        for index, customer in enumerate(customerList):
            footPrintCount = len(list(doc.id for doc in self.__usersCollection.document(customer['id']).collection('footPrintIds').stream()))
            customerList[index]['footPrintCount'] = footPrintCount
        return customerList

    def retrieveCustomerData(self):
        customerList = list(doc.to_dict() for doc in self.__usersCollection.where('role','==','customer').get())
        for index, customer in enumerate(customerList):
            footPrintCount = len(list(doc.id for doc in self.__usersCollection.document(customer['id']).collection('footPrintIds').stream()))
            customerList[index]['footPrintCount'] = footPrintCount
        return customerList

    def addSiteIdToUser(self,siteId,userId):
        self.__usersCollection.document(userId).collection('siteIds').document(siteId).set(None)

    def addFootPrintIdToUser(self,footPrintId,userId):
        self.__usersCollection.document(userId).collection('footPrintIds').document(footPrintId).set(None)

    def addFootPrintIdToSite(self,footPrintId,siteId):
        self.__sitesCollection.document(siteId).collection('footPrintIds').document(footPrintId).set(None)

    # def listFootPrintsDataOfUser(self, userId):
    #     footPrintIdsOfUser = list(doc.id for doc in self.__usersCollection.document(userId).collection('footPrintIds').stream())
    #     footPrintsDataOfUser = []
    #     for footPrintId in footPrintIdsOfUser:
    #         footPrintsDataOfUser.append(self.__footPrintsCollection.document(footPrintId).get().to_dict())

    #     return footPrintsDataOfUser

    def listSitesDataOfUser(self, userId):
        siteIdsOfUser = list(doc.id for doc in self.__usersCollection.document(userId).collection('siteIds').stream())
        sitesDataOfUser = []
        for siteId in siteIdsOfUser:
            sitesDataOfUser.append(self.__sitesCollection.document(siteId).get().to_dict())

        return sitesDataOfUser


    def uploadImage(self, img, fileName):
        img = cv2.cvtColor(numpy.asarray(img),cv2.COLOR_RGB2BGR)
        img = base64.b64encode(cv2.imencode('.jpg', img)[1]).decode()
        img = base64.b64decode(img)
        bucket = storage.bucket()
        blob = bucket.blob('siteQrCode/' + fileName)
        blob.upload_from_string(img)
        imageUrl = blob.public_url
        blob.make_public()

        return imageUrl
    def delete_collection(self, coll_ref):
        docs = coll_ref.stream()

        for doc in docs:
            doc.reference.delete()

    def deleteAllFootprintData(self):
        self.delete_collection(self.__footPrintsCollection)

        for doc in self.__usersCollection.stream():
            self.delete_collection(doc.reference.collection('footPrintIds'))

        for doc in self.__sitesCollection.stream():
            self.delete_collection(doc.reference.collection('footPrintIds'))

    def listFootPrintsDataOfUserByTime(self, userId, beginTime):
        footPrintIdsOfUser = list(doc.id for doc in self.__usersCollection.document(userId).collection('footPrintIds').stream())
        footPrintsDataOfUser = []
        for footPrintId in footPrintIdsOfUser:
            footprintData = self.__footPrintsCollection.document(footPrintId).get().to_dict()
            if footprintData["timestamp"]>=beginTime:
                footPrintsDataOfUser.append(footprintData)
        return footPrintsDataOfUser

    def listFootPrintsDataOfSiteByTime(self, siteId, beginTime):
        footPrintIdsOfSite = list(doc.id for doc in self.__sitesCollection.document(siteId).collection('footPrintIds').stream())
        footPrintsDataOfSite = []
        for footPrintId in footPrintIdsOfSite:
            footprintData = self.__footPrintsCollection.document(footPrintId).get().to_dict()
            if footprintData["timestamp"]>=beginTime:
                footPrintsDataOfSite.append(footprintData)
        return footPrintsDataOfSite 

    def getUserDataById(self, userId):
        userData = self.__usersCollection.document(userId).get().to_dict()
        return userData


    def getSiteDataById(self, siteId):
        siteData = self.__sitesCollection.document(siteId).get().to_dict()
        return siteData

    def getFootprintDataById(self, footprintId):
        footprintData = self.__footPrintsCollection.document(footprintId).get().to_dict()
        return footprintData

    def getProviderDataById(self, providerId):
        providerData = self.__providersCollection.document(providerId).get().to_dict()
        return providerData

    def getAllFootprintData(self):
        footprintList =self.__footPrintsCollection.stream()
        footprintsDataList = []
        for doc in footprintList:
            footprintsDataList.append(doc.to_dict())
            # print(f'{doc.id} => {doc.to_dict()}')
        return footprintsDataList

    def getAllUserData(self):
        userList =self.__usersCollection.stream()
        userDataList = []
        for doc in userList:
            userDataList.append(doc.to_dict())
            # print(f'{doc.id} => {doc.to_dict()}')
        return userDataList

    def getAllSiteData(self):
        siteList =self.__sitesCollection.stream()
        siteDataList = []
        for doc in siteList:
            siteDataList.append(doc.to_dict())
            # print(f'{doc.id} => {doc.to_dict()}')
        return siteDataList

    def addResultToBuffer(self, dataModel):
        self.__firestore.collection('buffer').document("ContactTracing").set(dataModel)
