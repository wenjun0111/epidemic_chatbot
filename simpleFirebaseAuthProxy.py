import firebaseInit
from firebase_admin import auth
import requests
import random
import string
from richMenu_handler import dynamic_create_richMenu,delete_rich_menu
from model import UserModel,UserResponse

def createUser(userModel , richMenuModel):
    password=''.join(random.sample(string.ascii_letters+string.digits,12))
    try:
        user = auth.create_user(
            email=userModel["email"],
            email_verified=True,
            phone_number=userModel["phone"],
            password=password,
            display_name=userModel["name"],
            disabled=False)
        # auth.create_user() first , create scuess ,try or except 
        richMenuModel = dynamic_create_richMenu(richMenuModel)
        #print(richMenuModel)
        custom={
            "user": {
                "data": {
                    "role": userModel["role"],
                    "lineId": userModel["lineId"],
                    "menu": richMenuModel["menu"],
                    #"submenu": richMenuModel["submenu"],
                }
            }
        }
        auth.set_custom_user_claims(user.uid,custom)
    except Exception as e:
        print("create error: " , e)
        error_msg = str(e)
        error_msg = error_msg.split(" ")[1]
        print(error_msg)
        return False , error_msg
    return True , "0"

def deleteUser(email):
    try:
        userData = getAuthUserByemail(email)
        uid = list(userData.keys())[0]
        delete_rich_menu(userData[uid]['menu']["Id"])
        #delete_rich_menu(userData[uid]['submenu']["Id"])
        auth.delete_user(uid, app=None)
    except Exception as e:
        print("delete error: " , e)
        return False
    return userData[uid]
def updateUser_s(uid,userModel,richMenuModel):
    try:
        delete_rich_menu(richMenuModel['menu']["Id"])
        delete_rich_menu(richMenuModel['submenu']["Id"])
        richMenuModel['menu']["Id"] = None
        richMenuModel['submenu']["Id"] = None
        richMenuModel = dynamic_create_richMenu(richMenuModel)

        auth.update_user(uid,            
            email=userModel["email"],
            email_verified=True,
            phone_number=userModel["phone"],
            display_name=userModel["name"],
            disabled=False,
            custom_claims={
                "user": {
                    "data": {
                    "role": userModel["role"],
                    "lineId": userModel["lineId"],
                    "menu": richMenuModel["menu"],
                    #"submenu": richMenuModel["submenu"],
                    }
                }
            })
    except Exception as e:
        print("update error: " , e)
        error_msg = str(e)
        error_msg = error_msg.split(" ")[1]
        print(error_msg)
        return False , error_msg
    return True , "0"

def getAuthUserByemail(email):
    try:
        UserRecord = auth.get_user_by_email(email, app=None)
        uid = UserRecord._data["localId"]
        userData = getAuthUserById(uid)
    except Exception as e:
        print("getAuthUserByemail error: " , e)
        return False
    return userData
def getuidFromuserData(userData):
    uid = list(userData.keys())
    return uid
def getlineidFromuserData(uid_list,userData,role):
    role_list = []
    for uid in uid_list:
        if userData[uid]['role'] == role:
            print(userData[uid])
            role_list.append(userData[uid]['lineId'])
    return role_list
def getAuthUserById(uid):
    userData={}
    user=auth.get_user(uid)
    userData[user.uid]={}
    userData[user.uid]['uid']=user.uid
    userData[user.uid]['display_name']=user.display_name
    userData[user.uid]['email']=user.email
    userData[user.uid]['phone_number']=user.phone_number
    userData[user.uid]['lineId']=user.custom_claims['user']['data']['lineId']
    userData[user.uid]['role']=user.custom_claims['user']['data']['role']
    userData[user.uid]['menu'] = {}
    userData[user.uid]['submenu'] = {}
    userData[user.uid]['menu']["Id"]=user.custom_claims['user']['data']['menu']["Id"]
    #userData[user.uid]['submenu']["Id"]=user.custom_claims['user']['data']['submenu']["Id"]
    return userData 

def getAuthUserBylineId(lineId):
    userData={}
    users=auth.list_users()
    for user in users.users:
        if(user.custom_claims['user']['data']['lineId']==lineId):
            userData={}
            try:
                userData['uid']=user.uid
                userData['display_name']=user.display_name
                userData['email']=user.email
                userData['phone_number']=user.phone_number
                userData['role']=user.custom_claims['user']['data']['role']
                userData['lineId']=user.custom_claims['user']['data']['lineId']
                userData['creation_timestamp']=user.user_metadata.creation_timestamp
                return userData
            except:
                print("Wrong User")
                return False
            
def getAuthUsers():
    listData={}
    users=auth.list_users()
    for user in users.users:
        if(user.email_verified==True):
            listData[user.uid]={}
            try:
                listData[user.uid]['uid']=user.uid
                listData[user.uid]['display_name']=user.display_name
                listData[user.uid]['email']=user.email
                listData[user.uid]['phone_number']=user.phone_number
                listData[user.uid]['role']=user.custom_claims['user']['data']['role']
                listData[user.uid]['lineId']=user.custom_claims['user']['data']['lineId']
            except:
                print("Wrong User")
    return listData



            




